﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class sklad : Form
    {
        public static int id1 = 0, id2 = 0, numdel = 0, numdel2=0;
        public static String qdel = "";
        public sklad()
        {
            InitializeComponent();
            dataGridView1.DataSource = controller.Query("select * from product order by idproduct;");
            dataGridView1.Columns[0].HeaderText = "№";
            dataGridView1.Columns[0].Width = 26;
            dataGridView1.Columns[1].HeaderText = "Название";
            dataGridView1.Columns[2].HeaderText = "Тип";
            dataGridView1.Columns[3].HeaderText = "Цена";
            dataGridView1.Columns[4].HeaderText = "Количество";
            dataGridView2.DataSource = controller.Query("select * from tare order by idtare;");
            dataGridView2.Columns[0].HeaderText = "№";
            dataGridView2.Columns[0].Width = 26;
            dataGridView2.Columns[1].HeaderText = "Название";
            dataGridView2.Columns[2].HeaderText = "Цена";
            dataGridView2.Columns[3].HeaderText = "Количество";
            dataGridView2.Columns[4].HeaderText = "Время";
            id1 = dataGridView1.RowCount;
            id2 = dataGridView2.RowCount;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (numdel != 0)
                controller.Query1(qdel);
            int i = 1;
            DataTable changes = ((DataTable)dataGridView2.DataSource).GetChanges();

            foreach (DataGridViewRow r in dataGridView2.Rows)
            {
                i++;
                if (i <= id1)
                {
                    if (changes != null)
                    {
                        controller.Query1("update tare set name='" + r.Cells["name"].Value + "', price='" + r.Cells["price"].Value + "', time='" + r.Cells["time"].Value + "', amount='" + r.Cells["amount"].Value + "' where idtare='" + r.Cells["idtare"].Value + "';");
                    }
                }
                else
                {
                    if (changes != null)
                    {
                        controller.Query1("INSERT INTO tare (idtare, name, price, time, amount) VALUES ('" + r.Cells["idtare"].Value + "', '" + r.Cells["name"].Value + "', '" + r.Cells["price"].Value + "', '" + r.Cells["time"].Value + "', '" + r.Cells["amount"].Value + "');");
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (numdel != 0)
                controller.Query1(qdel);
            int i = 1;
            DataTable changes = ((DataTable)dataGridView1.DataSource).GetChanges();

            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                i++;
                if (i <= id1)
                {
                    if (changes != null)
                    {
                        controller.Query1("update product set name='" + r.Cells["name"].Value + "', price='" + r.Cells["price"].Value + "', type='" + r.Cells["type"].Value + "', amount='" + r.Cells["amount"].Value + "' where idproduct='" + r.Cells["idproduct"].Value + "';");
                    }
                }
                else
                {
                    if (changes != null)
                    {
                        controller.Query1("INSERT INTO product (idproduct, name, price, type, amount) VALUES ('" + r.Cells["idproduct"].Value + "', '" + r.Cells["name"].Value + "', '" + r.Cells["price"].Value + "', '" + r.Cells["type"].Value + "', '" + r.Cells["amount"].Value + "');");
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = controller.Query("select * from product order by idproduct;");
            id1 = dataGridView1.RowCount;
            numdel = 0;
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            numdel++;
            int id = e.RowIndex;
            qdel += "DELETE FROM `product` WHERE `idproduct`='" + id + "'; ";
        }
        private void dataGridView2_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            numdel2++;
            int id = e.RowIndex;
            qdel += "DELETE FROM `tare` WHERE `idtare`='" + id + "'; ";
        }
        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = controller.Query("select * from tare order by idtare;");
            id1 = dataGridView1.RowCount;
            numdel = 0;
        }

    }
}
