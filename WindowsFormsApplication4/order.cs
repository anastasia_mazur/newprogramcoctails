﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class order : Form
    {
        public order()
        {
            InitializeComponent();
            comboBox1.DataSource = controller.Query("select name from coctail order by name;");
            comboBox1.DisplayMember = "name";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(textBox2.Text) < Convert.ToDouble(textBox1.Text))
                MessageBox.Show("Сумма оплаты меньше суммы заказа");
            else
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                finish f = new finish();
                this.Visible = false;
                f.ShowDialog();
                this.Close();
            }
        }
        public DataGridView ShiptoGrid(DataGridView datagrid)
        {
            if (datagrid.ColumnCount == 0)
            {
                datagrid.Columns.Add("name", "Название");
                datagrid.Columns.Add("amount", "Количество");
                datagrid.Columns.Add("price", "Цена");
            }
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                datagrid.Rows.Add(
                    row.Cells[0].Value,
                    row.Cells[1].Value,
                    row.Cells[2].Value
                    );
            }
            return datagrid;
        }

        private void user_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
        int i = 0;
        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dtable = new DataTable();
            dtable = controller.Query("select coctail.price from coctail where name = '" + comboBox1.GetItemText(comboBox1.SelectedItem) + "';");
            double num = Convert.ToDouble(textBox1.Text);
            num += Convert.ToDouble(dtable.Rows[0].ItemArray[0]);
            int n = 0, id = 0, n2 = 0;
            textBox1.Text = num.ToString();
            double price = Convert.ToDouble(dtable.Rows[0].ItemArray[0]);
            for (int b = 0; b < i; b++)
            {
                if (dataGridView1.Rows[b].Cells[0].Value.ToString() == comboBox1.GetItemText(comboBox1.SelectedItem))
                {
                    n++;
                    id = b;
                    n2 += Convert.ToInt32(dataGridView1.Rows[id].Cells[1].Value);
                }
            }
            if (n == 0)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].Cells[0].Value = comboBox1.GetItemText(comboBox1.SelectedItem);
                dataGridView1.Rows[i].Cells[1].Value = 1;
                dataGridView1.Rows[i].Cells[2].Value = price;
                i++;
            }
            else
            {
                n2 += 1;
                dataGridView1.Rows[id].Cells[1].Value = n2;
                dataGridView1.Rows[id].Cells[2].Value = price * n2;
            }
            
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            DataTable dt = new DataTable();
            dt = controller.Query("select coctail.price from coctail where name = '" + dataGridView1.Rows[i].Cells[0].Value + "';");
            double pr = Convert.ToDouble(dt.Rows[0].ItemArray[0]);
            dataGridView1.Rows[i].Cells[2].Value = Convert.ToInt32(dataGridView1.Rows[i].Cells[1].Value) * pr;
        }

        private void order_FormClosed(object sender, FormClosedEventArgs e)
        {
            i = 0;
        }
    }
}
