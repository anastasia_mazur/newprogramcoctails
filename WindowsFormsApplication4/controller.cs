﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication4
{
    class controller
    {
        static string connectionString = "Server=localhost;" +
                    "Database=coctailbar;" +
                    "Uid=root;" +
                    "Pwd=0000;";
        public static DataTable Query(String q1)
        {
            MySqlConnection MyConn = new MySqlConnection(connectionString);
            MySqlCommand MyCommand = new MySqlCommand(q1, MyConn);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(q1, MyConn);
            dataAdapter.SelectCommand = new MySqlCommand(q1, MyConn);
            DataTable dTable = new DataTable();
            dataAdapter.Fill(dTable);
            return dTable;
        }
        public static void Query1(String q1)
        {
            try
            {
                MySqlConnection MyConn = new MySqlConnection(connectionString);
                MySqlCommand MyCommand = new MySqlCommand(q1, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyCommand.ExecuteReader();
                MyConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
