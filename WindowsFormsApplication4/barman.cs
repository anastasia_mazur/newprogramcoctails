﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class barman : Form
    {
        DataGridView dgv = new DataGridView();
        public barman(DataGridView dgv)
        {
            InitializeComponent();
            this.dgv = dgv;
            foreach (DataGridViewRow row in dgv.Rows)
            {
                dataGridView1.Rows.Add(
                    row.Cells[0].Value,
                    row.Cells[1].Value,
                    row.Cells[2].Value
                    );
            }
            comboBox1.DataSource = controller.Query("select name from `coctail` order by name;");
            comboBox1.DisplayMember = "name";
            dataGridView2.DataSource = controller.Query("select distinct product.name, prodam from `product`, recipe, coctail where recipe.prodid = product.idproduct and cocid = (select idcoctail from coctail where coctail.name='" + comboBox1.GetItemText(comboBox1.SelectedItem) + "');");
            dataGridView2.Columns[0].HeaderText = "Название";
            dataGridView2.Columns[1].HeaderText = "Количество";
            DataTable dt = new DataTable();
            dt = controller.Query("select distinct tare.name, coctail.price from tare, coctail where (select tareid from coctail where coctail.name='" + comboBox1.GetItemText(comboBox1.SelectedItem) + "') = tare.idtare;");
            textBox1.Text = dt.Rows[0].ItemArray[0].ToString();
            textBox2.Text = dt.Rows[0].ItemArray[1].ToString();
        }
        public barman()
        {
            InitializeComponent();
            comboBox1.DataSource = controller.Query("select name from `coctail` order by name;");
            comboBox1.DisplayMember = "name";
            dataGridView2.DataSource = controller.Query("select distinct product.name, prodam from `product`, recipe, coctail where recipe.prodid = product.idproduct and cocid = (select idcoctail from coctail where coctail.name='" + comboBox1.GetItemText(comboBox1.SelectedItem) + "');");
            dataGridView2.Columns[0].HeaderText = "Название";
            dataGridView2.Columns[1].HeaderText = "Количество";
            DataTable dt = new DataTable();
            dt = controller.Query("select distinct tare.name, coctail.price from tare, coctail where (select tareid from coctail where coctail.name='" + comboBox1.GetItemText(comboBox1.SelectedItem) + "') = tare.idtare;");
            textBox1.Text = dt.Rows[0].ItemArray[0].ToString();
            textBox2.Text = dt.Rows[0].ItemArray[1].ToString();
        }
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.GetItemText(comboBox1.SelectedItem) != "System.Data.DataRowView")
            {
                dataGridView2.DataSource = controller.Query("select distinct product.name, prodam from `product`, recipe, coctail where recipe.prodid = product.idproduct and cocid = (select idcoctail from coctail where coctail.name='" + comboBox1.GetItemText(comboBox1.SelectedItem) + "');");
                DataTable dt = new DataTable();
                dt = controller.Query("select distinct tare.name, coctail.price from tare, coctail where (select tareid from coctail where coctail.name='" + comboBox1.GetItemText(comboBox1.SelectedItem) + "') = tare.idtare;");
                textBox1.Text = dt.Rows[0].ItemArray[0].ToString();
                textBox2.Text = dt.Rows[0].ItemArray[1].ToString();
            }
        }

    }
}
